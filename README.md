# NGinx

- Install: https://www.nginx.com/resources/wiki/start/topics/tutorials/install/
- `nano /etc/apt/sources.list.d/nginx.list`

```
deb http://nginx.org/packages/ubuntu/ xenial nginx
deb-src http://nginx.org/packages/ubuntu/ xenial nginx
```

- `sudo apt update`
- `sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys $key` (`$key` in error)
- `sudo apt update`
- `sudo apt install nginx`
- `sudo service nginx start`
- `cd /srv/www`
- `git clone …`
- `cd …`
- `nano /etc/nginx/nginx.conf` http://nginx.org/en/docs/beginners_guide.html (*Static file serving* section)

```nginx
http {
    server {
        # Let's Encrypt
        location ~ /.well-known {
            allow all;
        }
        
        location / {
            root /srv/musicblackholes.com;
        }
    }
# …
```

- `nginx -s reload`

## Point a domain

- Change DNS `A` record to point to the host IP address
- Use https://www.dnscheck.co to get propagation notification e-mail

## Let's Encrypt

(Do not do this, use Dokku and the LE plugin instead.)

- `cd /home`
- `git clone github.com/certbot/certbot`
- `cd certbot`
- `./letsencrypt-auto certonly -a manual -d …`

In a new terminal (for manual verification):

- `ssh`
- `cd /srv/www/…`
- `mkdir -p .well-known/acme-challenge`
- `cd .well-known/acme-challenge`
- `nano` and make the challenge file
- Continue with the `lets-encrypt` auto terminal
